<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\logger\driver\dbLogger
{
	use nuclio\plugin\database\orm\ORM;
	use nuclio\core\ClassManager;
	use nuclio\plugin\logger\psr3\LoggerInterface;
	use nuclio\plugin\database\datasource\manager\Manager as DataSourceManager;

	<<provides('logger::dbLogger')>>
	class DbLogger implements LoggerInterface
	{

		private $model;
		private $path;
		/**
		 * @var Monolog\Logger|null
		 */
		private Logger $logger;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Mongo
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(Map<string,string> $options=Map{})
		{
			$source  = $options->get('channel')['source'];
			$ORM=ORM::getInstance();
			$ORM->bindDataSource(DataSourceManager::getSource($source));
			$this->logger  = $options->get('channel')['logger'];
			$this->path = $options->get('channel')['path'];
			// $this->model = ORM::create($this->path,Map{'logger'=>$this->logger});
	 	}
	 	
	 	/**
		 * @inheritdoc
		 */
		public function emergency(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'emergency',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function alert(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'alert',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function critical(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'critical',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
 		/**
		 * @inheritdoc
		 */
		public function error(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'error',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function warning(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'warning',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function notice(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'notice',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function info(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'info',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function debug(string $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'debug',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
		
		/**
		 * @inheritdoc
		 */
		public function log(int $level, $message, array<mixed> $context=[]):this
		{
			$this->model=ORM::create
			(
				$this->path,
				Map
				{
					'logger'	=>$this->logger,
					'action'	=>'log',
					'info'		=>$message,
					'datetime'	=>time(),
					'context'	=>$context
				}
			);
			$this->model->save();
			return null;
		}
	}
}
